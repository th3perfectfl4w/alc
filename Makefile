CC= javac
CFLAGS= -cp
ifeq ($(OS),Windows_NT)
    WEKA= '.;../json-20160212.jar;../weka.jar'
else
    WEKA= ".:../json-20160212.jar:../weka.jar"
endif
SRC = src
ALC= alc
UTILITY = utility
THIS_FILE := $(lastword $(MAKEFILE_LIST))

all: Driver

#Executibles
Driver: $(SRC)/$(ALC)/Driver.java
	@echo Compiling Driver
	cd $(SRC); \
	$(CC) $(CFLAGS) $(WEKA) -Xlint:unchecked $(ALC)/$@.java; \
	echo Launching Application; \
	java $(CFLAGS) $(WEKA) $(ALC)/$@
	@$(MAKE) -f $(THIS_FILE) clean # invoke other target

RecipePuller: $(SRC)/$(UTILITY)/RecipePuller.java
	@echo Compiling Recipe Puller
	cd $(SRC); \
	$(CC) $(CFLAGS) $(WEKA) -Xlint:unchecked $(UTILITY)/$@.java; \
	echo Launching Application; \
	java $(CFLAGS) $(WEKA) $(UTILITY)/$@
	@$(MAKE) -f $(THIS_FILE) clean # invoke other target
	

#Tests

#clean-up
clean:
	cd $(SRC); \
	echo Cleanup; \
	rm -rf */*.class;

