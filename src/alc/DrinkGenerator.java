package alc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

import org.json.JSONObject;
import org.json.JSONArray;

import weka.core.converters.ConverterUtils.DataSource;
import weka.core.Instances;
import weka.classifiers.Classifier;
import weka.classifiers.functions.MultilayerPerceptron;;

public class DrinkGenerator {
	String LIGet = "http://addb.absolutdrinks.com/drinks/long-island-iced-tea/?apiKey=d1155f1d75714deaaef1a72449020c95";
	
	public void DrinkGenerator() {
// TODO take constraints as input		
	}
	
	public JSONObject makeDrink() throws Exception {
		DataSource source = new DataSource("InputDrinks.arff");
		Instances data = source.getDataSet();
		
		// See http://weka.sourceforge.net/doc.dev/weka/classifiers/functions/MultilayerPerceptron.html for javadoc
		MultilayerPerceptron neuralnet = new MultilayerPerceptron();
		
		// Builds a neural network from the training data provided
		neuralnet.buildClassifier(data);
		

		// TODO actually make a drink
		
		URL obj = new URL(LIGet);
		
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String line;
		StringBuffer response = new StringBuffer();
		
        String drinkStr = in.readLine();
        JSONObject drinkFull = new JSONObject(drinkStr);

        JSONObject drink = null;
        drink = drinkFull.getJSONArray("result").getJSONObject(0);
        
        /*Iterator<String> keys = drink.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            System.out.println(key);
        }
*/

		//while ((line = in.readLine()) != null) {
		//	System.out.println(line);
		//}
		
		return drink;
	}
}
