package alc;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import org.json.*;
import gui.LaunchWindow;

public class Driver {
	public static void main(String[] args) {
		System.out.println("Hello World");
		
		//exampleParse();
		LaunchWindow window = new LaunchWindow();
		window.getFrame().setVisible(true);
	}
	
	public static void exampleParse() {
		try {
			URL url = (new java.io.File("../ExampleJson.json")).toURI().toURL();
			JSONTokener tokener = new JSONTokener(url.openStream());
			JSONObject root = new JSONObject(tokener);
			
			Iterator<String> keys = root.getJSONObject("menu").keys();
			while (keys.hasNext()) {
				String key = keys.next();
				
				System.out.println(key);
			}
			
			System.out.println("done with this stupid example parse of json");
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
