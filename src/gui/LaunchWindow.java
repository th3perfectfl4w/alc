package gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import javax.swing.JSplitPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class LaunchWindow {

	private JFrame frmDrinkCreatorPrototype;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LaunchWindow window = new LaunchWindow();
					window.frmDrinkCreatorPrototype.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LaunchWindow() {
		initialize();
	}
	
	public JFrame getFrame() {
		return frmDrinkCreatorPrototype;
	}
	
	public void clickQuit() {
		System.out.println("clicked quit");
		System.exit(0);
	}

	public void clickStartDrink() {
		System.out.println("clicked start");
		frmDrinkCreatorPrototype.setContentPane(new PreQuestionPanel(frmDrinkCreatorPrototype));
		frmDrinkCreatorPrototype.revalidate();
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmDrinkCreatorPrototype = new JFrame();
		frmDrinkCreatorPrototype.setTitle("Drink Creator Prototype");
		frmDrinkCreatorPrototype.setBounds(100, 100, 450, 300);
		frmDrinkCreatorPrototype.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDrinkCreatorPrototype.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JLabel lblNewLabel = new JLabel("Welcome to the prototype of our drink creator.");
		frmDrinkCreatorPrototype.getContentPane().add(lblNewLabel, BorderLayout.NORTH);
		
		JSplitPane splitPane = new JSplitPane();
		frmDrinkCreatorPrototype.getContentPane().add(splitPane, BorderLayout.CENTER);
		
		JButton btnQuit = new JButton("Quit");
		btnQuit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				clickQuit();
			}
		});
		splitPane.setLeftComponent(btnQuit);
		
		JButton btnGetStarted = new JButton("Get Started");
		btnGetStarted.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				clickStartDrink();
			}
		});
		splitPane.setRightComponent(btnGetStarted);
	}

}
