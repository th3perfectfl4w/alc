package gui;

import javax.swing.JPanel;

import org.json.JSONObject;

import alc.DrinkGenerator;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PreQuestionPanel extends JPanel {

	JFrame frame;
	
	public void startDrink() {
		System.out.println("Starting Drink");
		DrinkGenerator gen = new DrinkGenerator();
		JSONObject drink = null;
		try {
			drink = gen.makeDrink();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		frame.setContentPane(new RecipeDisplayPanel(drink));
		frame.revalidate();
	}
	
	/**
	 * Create the panel.
	 */
	public PreQuestionPanel(JFrame frame) {
		this.frame = frame;
		
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{224, 2, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblNewLabel = new JLabel("Do you like strawberries?");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		add(lblNewLabel, gbc_lblNewLabel);
		
		JCheckBox chckbxYes = new JCheckBox("Yes");
		GridBagConstraints gbc_chckbxYes = new GridBagConstraints();
		gbc_chckbxYes.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxYes.gridx = 2;
		gbc_chckbxYes.gridy = 1;
		add(chckbxYes, gbc_chckbxYes);
		
		JLabel lblNewLabel_1 = new JLabel("How strong do you want it?");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 2;
		add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Beer Strength", "Wine Strength", "A Shot"}));
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 2;
		gbc_comboBox.gridy = 2;
		add(comboBox, gbc_comboBox);
		
		JLabel lblNewLabel_2 = new JLabel("Do you want an interesting color?");
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 3;
		add(lblNewLabel_2, gbc_lblNewLabel_2);
		
		JCheckBox chckbxYes_1 = new JCheckBox("Yes");
		GridBagConstraints gbc_chckbxYes_1 = new GridBagConstraints();
		gbc_chckbxYes_1.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxYes_1.gridx = 2;
		gbc_chckbxYes_1.gridy = 3;
		add(chckbxYes_1, gbc_chckbxYes_1);
		
		JButton btnStartDrink = new JButton("Start Drink");
		btnStartDrink.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO grab constraint information from gui
				startDrink();
			}
		});
		GridBagConstraints gbc_btnStartDrink = new GridBagConstraints();
		gbc_btnStartDrink.insets = new Insets(0, 0, 5, 5);
		gbc_btnStartDrink.gridx = 0;
		gbc_btnStartDrink.gridy = 10;
		add(btnStartDrink, gbc_btnStartDrink);
	}

}
