package gui;

import javax.swing.JPanel;

import org.json.JSONObject;
import org.json.JSONArray;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;

public class RecipeDisplayPanel extends JPanel {

	/**
	 * Create the panel.
	 */
	public RecipeDisplayPanel(JSONObject recipe) {
		setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		scrollPane.setViewportView(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		String[] labels = {"ingr 1", "ingr 2", "ingr 3"};
		String[] quants = {"quant 1", "quant 2", "quant 3"};
        JSONArray ingredients = recipe.getJSONArray("ingredients");
		for (int i = 0; i < ingredients.length(); i++) {
            JSONObject ingr = ingredients.getJSONObject(i);
            String name = ingr.getString("type");
            String quant = ingr.getString("textPlain");
           // String quant = "ingr Part";

			JLabel lblNewLabel = new JLabel(name);
			GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
			gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
			gbc_lblNewLabel.gridx = 0;
			gbc_lblNewLabel.gridy = i;
			panel.add(lblNewLabel, gbc_lblNewLabel);
			
			JLabel lblQuant = new JLabel(quant);
			GridBagConstraints gbc_lblQuant = new GridBagConstraints();
			gbc_lblQuant.insets = new Insets(0, 0, 5, 0);
			gbc_lblQuant.gridx = 3;
			gbc_lblQuant.gridy = i;
			panel.add(lblQuant, gbc_lblQuant);
		}
		
		JButton btnDone = new JButton("Done");
		GridBagConstraints gbc_btnDone = new GridBagConstraints();
		gbc_btnDone.insets = new Insets(0, 0, 0, 5);
		gbc_btnDone.gridx = 0;
		gbc_btnDone.gridy = 10;
		panel.add(btnDone, gbc_btnDone);

	}

}
