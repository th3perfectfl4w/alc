package utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * This is a tool to grab all the drinks from the absolut drink database and
 * add them to a csv file. This will allow us to train our neural net without
 * needing to fetch data from the api every time. The dataset is small enough
 * that it isn't worth the effort of pulling frequently. This can always be
 * re-run if they update the drinks and we need the updates.
 */

public class RecipePuller {
	// file with drink json
	static String InJSON = "InputDrinks.json";
	
	// file to write csv to
	static String OutputARFF = "InputDrinks.arff";
	
	// number of versions of each drink to make training data for
	static int versionsPer = 3;
	
	// we will throw away 25% of a drink to make training data
	static float amountOfDrinkToKeep = 0.75f;
	
	
	static String[] tests = {"2 Parts Agave Nectar", "1 Dash Orgeat Almond Syrup", "1 Half Lime", "1 Part Milk", "1 Splash Pisang Ambon"};
	static String[] toRemove = {"Part", "Parts"};
	static Map<String, String> conversions = new HashMap<String, String>();
	
	
	/**
	 * The first argument is the desired output file.
	 */
	public static void main(String[] args) throws Exception {
		String destFile = OutputARFF;
		if(args.length > 0) {
			destFile = args[0];
		}
		
		// TODO clean up, remove magic
		conversions.put("Splashes", "Splash");
		conversions.put("Dashes", "Dash");
		
		
		// starts here
		JSONObject drinks = null;
        BufferedReader drinkFileReader = new BufferedReader(new FileReader(InJSON));
        String wholeDrinkFile = drinkFileReader.readLine();
		drinks = new JSONObject(wholeDrinkFile);
        drinkFileReader.close();
		
		int numDrinks = drinks.getInt("totalResult");
		System.out.println(numDrinks);
		JSONArray drinkArr = drinks.getJSONArray("result");
		
		List<String> formattedDrinks = formatAllDrinks(drinkArr, numDrinks);
		
		// remove the identifier row to generate the attribute section
		String keyRow = formattedDrinks.remove(0);
		
		String[] individualKeys = keyRow.split(",");
		List<String> numericAtts = new ArrayList<String>();
		for (int i = 0; i < individualKeys.length - 1; i++) {
			numericAtts.add(individualKeys[i]);
		}
		
		// generate an exhaustive list of classes
		Set<String> uniqueDrinkNames = new HashSet<String>();
		String allDrinkNames = "";
		for (String drink : formattedDrinks) {
			String[] pieces = drink.split(",");
			uniqueDrinkNames.add(pieces[pieces.length - 1]);
		}
		
		for(String drinkName : uniqueDrinkNames) {
			allDrinkNames += drinkName + ",";
		}
		allDrinkNames = allDrinkNames.substring(0, allDrinkNames.length() - 1);
		
		printDrinksToFile(numericAtts, allDrinkNames, formattedDrinks, destFile);
	}

	
	
	public static List<String> formatAllDrinks(JSONArray drinks, int num) {
		Set<String> ingredientSet = new HashSet<String>(); // we use a set to avoid duplicates
		List<String> ingredientList = new ArrayList<String>(); // we then convert to a list to ensure the same order for each drink
		List<String> formattedDrinks = new ArrayList<String>();
		
		// grab every ingredient
		for (int i = 0; i < num; i++) {
			ingredientSet.addAll(getIngredients(drinks.getJSONObject(i)));
		}
		
		// create a consistent order for ingredients
		ingredientList.addAll(ingredientSet);
		
		// create a key row up top to make this readable
		String key = "";
		for (String ingr : ingredientList) {
			key += ingr + ", ";
		}
		key += "Drink Name";
		formattedDrinks.add(key);
		
		// using the consistent drink order, we create a row for the given drink
		for (int i = 0; i < num; i++) {
			formattedDrinks.addAll(getDrinkVersions(drinks.getJSONObject(i), ingredientList));
		}
		
		return formattedDrinks;
	}

	
	// gets all the ingredients in a drink
	public static List<String> getIngredients(JSONObject drink) {
		List<String> ingredients = new ArrayList<String>();
		JSONArray ingrArr = drink.getJSONArray("ingredients");
		int numIngredients = ingrArr.length();
		
		for (int i = 0; i < numIngredients; i++) {
			JSONObject oneIngr = ingrArr.getJSONObject(i);
			String ingrText = oneIngr.getString("textPlain");
			ingrText = ingrText.replace(',', '-');
			ingredients.add(getIngredientName(ingrText));
		}
		return ingredients;
	}

	
	// parses the ingredient name from the full ingredient String
	public static String getIngredientName(String fullIngredient) {
		// remove part information here
		String[] parts = fullIngredient.split(" ");
		String resultIngr = "";
					
		// correcting the words in the text
		for (int j = 0; j < parts.length; j++) {
			// ensure that it isn't a number
			if (!(Character.isDigit(parts[j].charAt(0)))) {
				// check to see if it is a word to be converted
				if (conversions.containsKey(parts[j])) {
					parts[j] = conversions.get(parts[j]);
				}
							
				// check to see if it is a word to remove
				for (int k = 0; k < toRemove.length; k++) {
					parts[j] = parts[j].equals(toRemove[k]) ? "" : parts[j];
				}
							
				resultIngr += parts[j] + " ";
			}
		}
					
		return resultIngr.trim();
	}
	
	
	// generates a training set for the given drink
	public static List<String> getDrinkVersions(JSONObject drink, List<String> allIngrs) {
		List<String> allVersions = new ArrayList<String>();
		
		String formatted = "";
		String name = drink.getString("id");
		
		JSONArray ingrArr = drink.getJSONArray("ingredients");
		int numIngredients = ingrArr.length();
		
		Map<String, Float> ingrQuant = new HashMap<String, Float>();
		float totalParts = 0;
		
		// count total number of parts
		for (int i = 0; i < numIngredients; i++) {
			JSONObject oneIngr = ingrArr.getJSONObject(i);
			String ingrText = oneIngr.getString("textPlain");
			ingrText = ingrText.replace(',', '-');
			
			// check if this is a parted ingredient
			String[] parts = ingrText.split(" ");
			for (int j = 0; j < parts.length; j++) {
				if (parts[j].equals("Part") || parts[j].equals("Parts")) {
					int ingrParts = 0;
					try {
						ingrParts = Integer.parseInt(parts[j - 1]);
					} catch (NumberFormatException e) {
						// IDK why this weird character comes up replacing numbers TODO
					}
					totalParts += ingrParts;
				}
			}
		}
		
		// re-tread through ingredients, storing ingredient quantities 
		for (int i = 0; i < numIngredients; i++) {
			JSONObject oneIngr = ingrArr.getJSONObject(i);
			String ingrText = oneIngr.getString("textPlain");
			String ingrName = getIngredientName(ingrText);
			
			String[] parts = ingrText.split(" ");
			
			boolean added = false;
			// add the ratio of parted ingredients (3 parts something)
			for (int j = 0; j < parts.length; j++) {
				if (parts[j].equals("Part") || parts[j].equals("Parts")) {
					added = true;
					int ingrParts = 0;
					try {
						ingrParts = Integer.parseInt(parts[j - 1]);
					} catch (NumberFormatException e) {
						// IDK why this weird character comes up replacing numbers TODO
					}
					ingrQuant.put(ingrName, new Float(ingrParts / totalParts));
				}
			}
			
			// add all non-parted ingredients (2 dashes something)
			if (!added && parts.length > 1) {
				try {
					int ingrAmt = Integer.parseInt(parts[0]);
					ingrQuant.put(ingrName, new Float(ingrAmt));
				} catch (NumberFormatException e) {
					// IDK why this weird character comes up replacing numbers TODO
				}
			}
		}
		
		
		for (int i = 0; i < versionsPer; i++) {
			allVersions.add(formatDrink(drink, allIngrs, name, ingrQuant));
		}

		return allVersions;
	}
	
	// removes various parts of the drink to make a piece of training data
	public static String formatDrink(JSONObject drink, List<String> allIngrs, String name, Map<String, Float> ingrQuant) {
		String formatted = "";
		
        Map<String, Float> dupedIngrQuant = new HashMap<String, Float>();
        int numIngrToKeep = (int)(ingrQuant.keySet().size() * amountOfDrinkToKeep);
        numIngrToKeep = numIngrToKeep == 0 ? 1 : numIngrToKeep;

        List<String> keyList = new ArrayList<String>();
        keyList.addAll(ingrQuant.keySet());
        while (numIngrToKeep > 0) {
            String toGet = keyList.remove((int)(Math.random() * keyList.size()));
            dupedIngrQuant.put(toGet, ingrQuant.get(toGet));
            numIngrToKeep--;
        }

		// iterate through all ingredients, building our row in the csv
		for (int i = 0; i < allIngrs.size(); i++) {
			String currIngr = allIngrs.get(i);
			String quant = "0";
			if (dupedIngrQuant.containsKey(currIngr)) {
				//quant = "" + dupedIngrQuant.get(currIngr);
                // we are just using the presence of the ingredient, ignoring quantity
				quant = "1";
			}
			
			formatted += quant + ", ";
		}
		formatted += name;
		
		return formatted;
	}
	
	
	public static void printDrinksToFile(List<String> numericAttributes, String allDrinkNames, List<String> drinks, String file) {
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(file, "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		writer.println("@RELATION drinks");
		writer.println("");
		
		for(String att : numericAttributes) {
			writer.println("@ATTRIBUTE " + att.trim().replace(' ', '-').replace("'", "") + " NUMERIC");
		}
		writer.println("@ATTRIBUTE " + "drink-name" + " {" + allDrinkNames + "}");
		writer.println("");
		
		writer.println("@DATA");
		for(String drnkStr : drinks) {
			writer.println(drnkStr);
		}
		writer.close();
	}
}
