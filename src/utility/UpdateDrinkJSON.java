package utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

/** 
 * Grabs the full drink database and makes it into a local json file. This can be run anytime or even from the 
 * training data generator. This was pulled out so that we don't always have to wait on the api to prepare training
 * data.
 */
public class UpdateDrinkJSON {
	// file to write json to
	static String OutputJSON = "InputDrinks.json";
	
	// Key for the ADD api
	static String APIKey = "?apiKey=d1155f1d75714deaaef1a72449020c95";
	
	// The page sizing is set up to grab all drinks, at present there are 3573 drinks.
	static String StartIndex = "&start=";
	static String SizeString = "&pageSize=";
	static int PageSize = 1000;
	
	// This is the url to get all the drinks from the database
	static String GetAllDrinks = "http://addb.absolutdrinks.com/drinks/";
	
	
	public static void main(String[] args) throws Exception {
		JSONObject allDrinks = getDrinks();
		
		PrintWriter writer = new PrintWriter(OutputJSON, "UTF-8");
		writer.println(allDrinks.toString());
		writer.close();
	}
	
	
	/**
	 * Grabs every drink from the drink database, returns the object formatted as
	 * {
	 *   "result": [],
	 *   "totalResult": 3573
	 * }
	 */
	public static JSONObject getDrinks() throws IOException {
		JSONObject retObj = new JSONObject();
		JSONArray drinkArr = new JSONArray();
		int startDrink = 0;
		int totalDrinks = -1;
		
		JSONObject drinkFull = null;
		
		// we grab 1000 drinks at a time, and create a new array holding all of them
		// the api limits the amount we can get in one call
		do {
			System.out.println(startDrink);
			String request = getXDrinksString(startDrink, PageSize);
			URL obj = new URL(request);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		
		
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String drinkStr = in.readLine();
			drinkFull = new JSONObject(drinkStr);
			JSONArray someDrinks = drinkFull.getJSONArray("result");
			
			for (int i = 0; i < someDrinks.length(); i++) {
				drinkArr.put(someDrinks.get(i));
			}
			
			totalDrinks = drinkFull.getInt("totalResult");
			startDrink += PageSize;
		} while (drinkFull.has("next"));
        
		retObj.put("result", drinkArr);
		retObj.put("totalResult", totalDrinks);
		
        return retObj;
	}
	
	public static String getXDrinksString(int startDrink, int numDrinks) {
		return GetAllDrinks + APIKey + StartIndex + startDrink + SizeString + numDrinks;
	}

}
